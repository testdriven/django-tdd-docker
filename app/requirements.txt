Django==5.0.7
dj-database-url==2.2.0
djangorestframework==3.15.2
drf-yasg==1.21.5
gunicorn==22.0.0
psycopg2-binary==2.9.9
whitenoise==6.7.0
