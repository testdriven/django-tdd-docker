black==24.4.2
flake8==7.1.0
isort==5.13.2
pytest==8.3.1
pytest-cov==5.0.0
pytest-django==4.8.0
pytest-xdist==3.6.1

-r requirements.txt
